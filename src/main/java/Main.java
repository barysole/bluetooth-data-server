import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalTime;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws IOException {
        UUID serviceUUID = UUID.fromString("41721a3f-c501-4ed8-b57f-dc981fe20f4a");
        System.out.println(serviceUUID);
        LocalDevice localDevice = LocalDevice.getLocalDevice();
        System.out.println("Bluetooth MAC:" + localDevice.getBluetoothAddress() + " UUID is " + serviceUUID);
        StreamConnectionNotifier stringProviderService = (StreamConnectionNotifier) Connector.open("btspp://" +
                "localhost" + ":" + "41721a3fc5014ed8b57fdc981fe20f4a" + ";");
        ExecutorService clientRunner = Executors.newCachedThreadPool();
        while (true) {
            final StreamConnection connection = stringProviderService.acceptAndOpen();
            System.out.println("===\nNew connection established at " + LocalTime.now().toString() + "\n===");
            clientRunner.execute(() -> {
                try {
                    OutputStream outputStream = connection.openOutputStream();
                    DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
                    Random random = new Random();
                    float generatedX = 0;
                    float generatedY;
                    float previousX;
                    int dataSize = random.nextInt(20) + 4;
                    dataOutputStream.writeInt(dataSize);
                    dataOutputStream.flush();
                    for (int i = 0; i < dataSize; i++) {
                        previousX = generatedX;
                        generatedX = random.nextInt(16) + previousX;
                        generatedY = random.nextFloat();
                        System.out.println("Coordinates {" + generatedX + "; " + generatedY + "} is sending...");
                        dataOutputStream.writeFloat(generatedX);
                        dataOutputStream.writeFloat(generatedY);
                        dataOutputStream.flush();
                    }
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
