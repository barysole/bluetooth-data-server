## DESCRIPTION
Bluetooth server application for the https://gitlab.fel.cvut.cz/barysole/svp-bluetooth-data-receiver

## BLUETOOTH LIBRARIES
Java has never had any standard implementations for Bluetooth support, therefore to ensure
Bluetooth communication a special library is needed. This library will work as a core of the server
application. However java has JSR-82 (was approved in 2000) that provides the specification for the
“javax.bluetooth” and “javax.obex” packages.
There is a pair of libraries that implement JSR-82 specification: Avetana
(https://sourceforge.net/projects/avetanabt/) and BlueCove (http://www.bluecove.org/). BlueCove
library used to develop by Google and has more descriptive documentation than Avetana, therefore
BlueCove was chosen as a main Bluetooth library for the server application. Unfortunately, this
project (as many of others) is inactivated for a long time and the last available version in maven
repository doesn’t support modern versions of Java, but google archive has version “2.1.1” that works
properly. There is no better platform-independent solution for Java at this time. Moreover BlueCove
2.1.1 distributed with “The Apache Software License, Version 2.0” so it can be used in this project.

## SENDING DATA DESCRIPTION
Server generates random data that is represented by several pairs of values with float type and sends
it to any recipient that has an established connection. For simplicity java server uses
“DataOutputStream” to send generated data, that means that the application writes primitive Java
data types to an output stream in a portable way.
There is a strongly-defined protocol that describes how data will be sent. First of all, the application
sends the float number that represents the number of float pairs which will be sent on the output
stream. After that the application sends a group of random float numbers.
